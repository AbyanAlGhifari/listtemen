var nilai1 = 1;
var nilai2 = 2;

function penjumlahan(a, b) {
    console.log(a+b);
}

penjumlahan(nilai1, nilai2);

function perkalian(a,b) {
    console.log(a*b);
}

perkalian(nilai1, nilai2);

function pembagian(a,b) {
    console.log(a/b);
}

pembagian(nilai1,nilai2);

function pengurangan(a,b) {
    console.log(a-b);
}

pengurangan(nilai1,nilai2);

function ratarata(a,b){
    console.log((a+b)/2);
}

ratarata(nilai1,nilai2);

//function expresi
const sayHi = function(nama){
    console.log("Hallo " + nama);
}

sayHi("Yan");

const bagi = function(nilai3, nilai4){
    console.log("Hasilnya " + nilai3 / nilai4);
}

bagi(10,2);

const kali = function(nilai5, nilai6){
    console.log("Hasilnya " + nilai5*nilai6);
}

kali(2,4);

const kurang = function(nilai7,nilai8) {
    console.log("Hasilnya "+ (nilai7-nilai8));
}

kurang(13,7);

const jumlah = function(nilai9,nilai10) {
    console.log("Hasilnya "+ (nilai9+nilai10));
}

jumlah(8,9);

const rata = function(nilai11,nilai12) {
    console.log("Hasilnya "+ (nilai11+nilai12)/2);
}

rata(20,2);